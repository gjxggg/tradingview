$(function() {
	var temp = 0
	var max_price = 0.00 // 最高买卖价
	var min_price = 0.00 // 最高买卖价
	var defaultPrice = 0.00 // 默认
	var rate = 0.03

	// 请求得封装头部
	var commomUrl = "http://103.143.158.93"; // 公共前缀url
	var commonHeader = { // 公共header
		"Accept": "application/json",
		"Authorization": "Bear " + "..."
	}

	//公共ajax封装
	function ajax(url, type, params, successfn, errorfn) {
		$.ajax({
			url: commomUrl + url,
			type: type,
			// headers: commonHeader,
			data: params,
			success: function(res) {
				successfn(res)
			},
			error: function(res) {
				// errorfn(res);
			},
			complete: function() {
				// console.log('结束 看需要写不写');
			}
		});
	}
	ajax('/api/get_k_line','POST',{
		'type':'1min',
		'coin':'BTC'
	})
	var getLang = function() {
		// var lang =  store.get('defaultLang');
		// switch (lang) {
		//     case 'cn':
		//         $('.Chinese').show()
		//         $('.English').hide()
		//         break;

		//      case 'en':
		//          $('.English').show()
		//          $('.Chinese').hide()
		//          break;
		// }
	}
	getLang()
	var getPrice = function() {
		// $.apiGet('Deal/getPrice', function (res) {
		//    var data = res.data
		//    max_price = Number(data.max_price)  // 最高买卖价
		//    min_price = Number(data.min_price)  // 最高买卖价
		//    defaultPrice = Number(data.price)  // 默认
		//    $("#Buyprice").val(defaultPrice)
		//    $("#Sellprice").val(defaultPrice)
		//    $('.defaultPrice').text(defaultPrice)
		//    $('.maxPrice').text(max_price)
		//    console.log('max_price',max_price,min_price,defaultPrice)

		// })
	}
	getPrice()
	var getNewBargain = function() {
		// $.apiGet('wcf/getMyUsdt', function (res) {
		//     //freeze_usdt 是冻结的 usdt 是可用的
		//     var freeze_usdt = Number(res.data.freeze_usdt).toFixed(0)
		//     var usdt = Number(res.data.usdt).toFixed(0)
		//     var all = parseInt(freeze_usdt) + parseInt(usdt)
		//     $('.freeze_usdt .value').text(freeze_usdt)
		//     $('.usdt .value').text(usdt)
		//     $('.all .value').text(all)
		// })
	}

	var getWCfBargain = function() {
		// $.apiGet('wcf/getMyWcf', function (res) {
		//     //freeze_usdt 是冻结的 usdt 是可用的
		//     var freeze_usdt = Number(res.data.freeze_wcf).toFixed(0)
		//     var usdt = Number(res.data.wcf).toFixed(0)
		//     var all = parseInt(freeze_usdt) + parseInt(usdt)
		//     $('.freeze_usdt .value').text(freeze_usdt)
		//     $('.usdt .value').text(usdt)
		//     $('.all .value').text(all)
		// })
	}

	getNewBargain()
	$(".exchange").on('click', function() {
		$(".exchange").addClass('active')
		$(".market").removeClass('active')
		setTimeout(() => {
			$(".exchange-cont").show()
			$(".market-cont").hide()
		}, 500);

	})
	$(".market").on('click', function() {
		$(".market").addClass('active')
		$(".exchange").removeClass('active')
		setTimeout(() => {

			$(".market-cont").show()
			$(".exchange-cont").hide()
		}, 500);

		if (temp == 0) {
			getKilne('3600')
			temp++
		}

	})

	$(".buy").on('click', function() { //买
		$(".buy").addClass('active')
		$(".sell").removeClass('active')
		$(".buy-cont").show()
		$(".sell-cont").hide()
		getNewBargain()
	})
	$(".sell").on('click', function() { //卖
		$(".sell").addClass('active')
		$(".buy").removeClass('active')
		$(".sell-cont").show()
		$(".buy-cont").hide()
		getWCfBargain()
	})

	$("#Buyprice").focus(function(event) { // 买入价格
		$("#Buyprice").val('')
		$("#Buyprice").addClass('focus-status')

	})
	$("#Buyprice").blur(function(event) { // 买入价格
		var price_buy = $("#Buyprice").val()
		if (price_buy == '' || price_buy == undefined) {
			$("#Buyprice").val((defaultPrice).toFixed(4))
		}
		$("#Buyprice").removeClass('focus-status')


	})

	$("#Sellprice").focus(function(event) { // 卖出价格
		$("#Sellprice").val('')
		$("#Sellprice").addClass('focus-status')

	})
	$("#Sellprice").blur(function(event) { // 卖出价格
		var price_sell = $("#Sellprice").val()
		if (price_sell == '' || price_sell == undefined) {
			$("#Sellprice").val((defaultPrice).toFixed(4))
		}
		$("#Sellprice").removeClass('focus-status')

	})

	$("#Buynum").focus(function(event) { // 买入数量
		$("#Buynum").val('')
		$("#Buynum").addClass('focus-status')

	})
	$("#Buynum").blur(function(event) { // 买入数量
		var num_buy = $("#Buynum").val()
		if (num_buy == '' || num_buy == undefined) {
			$("#Buynum").val(1)
			num_buy = 1
		}
		$("#Buynum").removeClass('focus-status')

	})

	$("#Sellnum").focus(function(event) { // 卖出数量
		$("#Sellnum").val('')
		$("#Sellnum").addClass('focus-status')

	})
	$("#Sellnum").blur(function(event) { // 卖出数量
		var num_sell = $("#Sellnum").val()
		if (num_sell == '' || num_sell == undefined) {
			$("#Sellnum").val(1)
			num_sell = 1
		}
		$("#Sellnum").removeClass('focus-status')


	})

	var rateVaule = 0
	var getRate = function() {
		$("#Buyprice").val((defaultPrice).toFixed(4))
		$("#Sellprice").val((defaultPrice).toFixed(4))
		$("#Buynum").val(1)
		// $("#Sellprice").val(1)
		$("#Sellnum").val(1)
		var Buyprice = $("#Buyprice").val()
		var Buynum = $("#Buynum").val()

		var Sellprice = $("#Sellprice").val()
		var Sellnum = $("#Sellnum").val()

		// $.apiGet('wcf/getRate', function (res) {
		//     rateVaule = Number(res.data.rate)
		//     var buyTrade1 = (Number(Buyprice) * Number(Buynum)).toFixed(2)
		//     var buyTrade = (Number(Buyprice) * Number(Buynum)*rate).toFixed(2)
		//     var buyAllCoin = (Number(buyTrade)+Number(buyTrade1)).toFixed(2)
		//     $('.buy_all_coin span').text(buyAllCoin)
		//     $('.buy_fee span').text(buyTrade)
		//     $('.buy-trade span').text(buyTrade1)
		//     var tradeSell = (Number(Sellprice) * Number(Sellnum)*rate).toFixed(2)
		//     var tradeSell1 = (Number(Sellprice) *Number(Sellnum)).toFixed(2)
		//     var sellAllCoin = (Number(tradeSell)+Number(tradeSell1)).toFixed(2)
		//     $('.trade-sell span').text(tradeSell1)
		//     $('.sell_fee span').text(tradeSell)
		//     $('.sell_all_coin span').text(sellAllCoin)
		// })
	}
	getRate()
	//买入价格  * 买入数量
	$("#Buyprice").bind("input propertychange", function(event) {
		var Buyprice = $("#Buyprice").val()
		var Buynum = $("#Buynum").val()
		// var buyTrade = (Number(Buyprice) * Number(Buynum)).toFixed(2)
		// // $('.buy-trade span').text(buyTrade)
		// var buyTrade1 = (Number(Buyprice) * Number(Buynum)).toFixed(2)

		// $('.buy-trade span').text(buyTrade1)
		// $('.buy_fee span').text((buyTrade*rate).toFixed(2))
		var buyTrade1 = (Number(Buyprice) * Number(Buynum)).toFixed(2)
		var buyTrade = (Number(Buyprice) * Number(Buynum) * rate).toFixed(2)
		var buyAllCoin = (Number(buyTrade) + Number(buyTrade1)).toFixed(2)
		$('.buy_all_coin span').text(buyAllCoin)
		$('.buy_fee span').text(buyTrade)
		$('.buy-trade span').text(buyTrade1)
	})
	$("#Buynum").bind("input propertychange", function(event) {
		var Buyprice = $("#Buyprice").val()
		var Buynum = $("#Buynum").val()
		// var buyTrade = (Number(Buyprice) * Number(Buynum)).toFixed(2)
		// var buyTrade1 = (Number(Buyprice) * Number(Buynum)).toFixed(2)

		// $('.buy-trade span').text(buyTrade1)

		// $('.buy_fee span').text((buyTrade*rate).toFixed(2))
		var buyTrade1 = (Number(Buyprice) * Number(Buynum)).toFixed(2)
		var buyTrade = (Number(Buyprice) * Number(Buynum) * rate).toFixed(2)
		var buyAllCoin = (Number(buyTrade) + Number(buyTrade1)).toFixed(2)
		$('.buy_all_coin span').text(buyAllCoin)
		$('.buy_fee span').text(buyTrade)
		$('.buy-trade span').text(buyTrade1)
	})
	//卖出数量  * 比例 * 价格
	$("#Sellprice").bind("input propertychange", function(event) {
		var Sellprice = $("#Sellprice").val()
		// if(Sellprice>defaultPrice){
		//     Sellprice = defaultPrice
		//     $("#Sellprice").val(defaultPrice)

		// }
		var Sellnum = $("#Sellnum").val()
		var tradeSell = (Number(Sellprice) * Number(Sellnum) * rate).toFixed(2)
		var tradeSell1 = (Number(Sellprice) * Number(Sellnum)).toFixed(2)
		var sellAllCoin = (Number(tradeSell) + Number(tradeSell1)).toFixed(2)
		$('.trade-sell span').text(tradeSell1)
		$('.sell_fee span').text(tradeSell)
		$('.sell_all_coin span').text(sellAllCoin)

	})
	$("#Sellnum").bind("input propertychange", function(event) {
		var Sellprice = $("#Sellprice").val()
		var Sellnum = $("#Sellnum").val()
		var tradeSell = (Number(Sellprice) * Number(Sellnum) * rate).toFixed(2)
		var tradeSell1 = (Number(Sellprice) * Number(Sellnum)).toFixed(2)
		var sellAllCoin = (Number(tradeSell) + Number(tradeSell1)).toFixed(2)
		console.log('sellAllCoin', sellAllCoin)
		$('.trade-sell span').text(tradeSell1) //交易额
		$('.sell_fee span').text(tradeSell)
		$('.sell_all_coin span').text(sellAllCoin)
	})

	var dataRender = function(list) {
		if (list.length <= 0) {
			$('#listView').hide().hideLoading();
			$('.not-data').show();
			return;
		}
		for (var i = 0; i < list.length; ++i) {

			var item = list[i];
			item.index = i + 1
			item.lang = store.get('defaultLang')
			var template = $(doT.template($("#itemTpl").text())(item));
			template.appendTo($('#listView'));
		}
		$('#listView').show();
	}

	var dataRender1 = function(list) {
		if (list.length <= 0) {
			$('#listView1').hide().hideLoading();
			$('.not-data').show();
			return;
		}
		for (var i = 0; i < list.length; ++i) {
			var item = list[i];
			item.index = i + 1
			item.lang = store.get('defaultLang')
			var template = $(doT.template($("#itemTpl1").text())(item));
			template.appendTo($('#listView1'));
		}
		$('#listView1').show();
	}

	var GetOrderList = function() {
		// $.apiGet('wcf/GetOrderList', function (res) {
		//     var getListbuy = []
		//     var getListsell = []
		//     // for (var key in res.data.buy_list) {
		//     //     res.data.buy_list[key].is_type = 1
		//     //     getList.push(res.data.buy_list[key])
		//     // }
		//     // for (var key in res.data.sell_list) {
		//     //     res.data.sell_list[key].is_type = 2
		//     //     getList.push(res.data.sell_list[key])
		//     // }
		//     if(res.data.buy_list.length>0){
		//         $('.buy-thead').show()
		//     }
		//     if(res.data.sell_list.length>0){
		//         $('.sell-thead').show()
		//     }
		//     $('#listView').html('').hide();
		//     $('#listView1').html('').hide();
		//     dataRender(res.data.buy_list);
		//     dataRender1(res.data.sell_list);
		//     var buyPrice = 0.00
		//     var sellPrice = 0.00
		//     if (res.data.buy_list.length > 0) {
		//         buyPrice = res.data.buy_list[0].price
		//     }
		//     if (res.data.sell_list.length > 0) {
		//         sellPrice = res.data.sell_list[0].price

		//     }
		//     // $("#Buyprice").val(Number(sellPrice).toFixed(4))
		//     // $("#Sellprice").val(Number(sellPrice).toFixed(4))
		//     $(".sellPrice").val(Number(sellPrice).toFixed(6))
		//     $(".buyPrice").val(Number(buyPrice).toFixed(6))
		//     // dataRender(getList);
		// });

	}


	var dataRender2 = function(list) {
		if (list.length <= 0) {
			$('#listView2').hide().hideLoading();
			$('.not-data').show();
			return;
		}
		for (var i = 0; i < list.length; ++i) {
			var item = list[i];
			item.index = i + 1
			item.lang = store.get('defaultLang')
			var template = $(doT.template($("#itemTpl2").text())(item));
			template.appendTo($('#listView2'));
		}
		$('#listView2').show();
	}
	var _options = {
		page: 1, // 页码
		flag: false, // 事件锁
	};
	var GetNewBargain = function() {
		// $.apiGet('wcf/getNewBargain', {page: _options.page}, function (res) {
		//     if (_options.flag) return;
		//     _options.flag = true;
		//     $('#listView2').loading();
		//     var getList = res.data.list.data
		//     for (var i = 0; i < getList.length; i++) {
		//         getList[i].deal_count_num = Number(getList[i].deal_count_num).toFixed(0)
		//         getList[i].buy_num = Number(getList[i].buy_num).toFixed(0)
		//         getList[i].buy_price = Number(getList[i].buy_price).toFixed(6)
		//         getList[i].create_time = ((getList[i].create_time).split(" "))[1]
		//         getList[i].allUsdt = (Number(getList[i].buy_num)*Number(getList[i].buy_price)).toFixed(4)
		//     }
		//     dataRender2(getList);
		//     if (_options.page >= res.data.list.last_page) {
		//         $('body').rollPage('destroy');
		//         $('#listView2').hideLoading();
		//         return;
		//     }
		//     _options.page++;
		//     _options.flag = false;

		// });

	}
	$.loadData = function() {
		$('#listView2').html('').hide();
		_options.page = 1;
		_options.flag = false;
		$('.not-data').hide();
		GetNewBargain();
		// $('body').rollPage('load', function () {
		//     GetNewBargain();
		// });
	}

	$.loadData();


	var submit = function(price, num, type) {
		var lock = false;
		var userInfo = $.getUserInfo();
		if (userInfo.set_business_password != 1) {
			$.confirm($.getI18n('JPASD1'), function() {
				location.href = "set-password.html";
				return false;
			});
		} else {
			$.dialog({
				title: $.getI18n('JPASD2'),
				html: '<div class="sui-dialog-input"><input type="password" i18n="JPASD2" placeholder="请输入交易密码" /></div>',
				autoClose: false,
				buttons: [{
					text: $.getI18n('JPASD3'),
					onClick: function() {
						$.closeDialog();
					}
				}, {
					text: $.getI18n('JPASD4'),
					onClick: function(data) {
						// 判断交易密码是否为空
						if (data.length <= 0) {
							$('.sui-dialog-input input').focus();
							return;
						}
						if (lock) return;
						lock = true;
						$.closeDialog();
						// 验证交易密码
						$.showLoading();
						$.apiRequest('Member/validBusinessPassword', {
							password: md5(data)
						}, function(res) {
							recharge(price, num, type, res.data.businessPasswordToken);
						}, function(res) {
							lock = false
							$.alert(res.message);
						});
					}
				}]
			});
		}
	}
	var recharge = function(price, num, type, PasswordToken) {
		$.showLoading();
		$.apiRequest('Wcf/CoinDal', {
			price: price,
			num: num,
			type: type,
			deal_coin_type: 'USDT/WCF'
		}, function(res) {
			$.toastSuccess("下单成功");
			$("#listView").empty();
			GetOrderList()
		}, function(res) {
			lock = false
			$.alert(res.message);
		});
	}

	/*
	 * price 价格 num数量
	 * type订单类型 1买单 2卖单   deal_coin_type 写死  USDT/WCF
	 */
	$("#buySubmit").on('click', function() { // 买出
		var type = 1 //type订单类型 1买单 2卖单 
		var price = $('#Buyprice').val();
		var num = $('#Buynum').val();
		if (price.length <= 0) {
			$.toast('请输入买入价格');
			return false;
		}

		if (num.length <= 0) {
			$.toast('请输入买入数量');
			return false;
		}
		if (Number(num) < 10) {
			$.toast('买入数量不少于10');
			return false;
		}
		if (Number(price) > max_price) {
			$.toast('买入价格不能大于' + max_price);
			return false;
		}
		if (Number(price) < min_price) {
			$.toast('买入价格不能小于' + min_price);
			return false;
		}

		submit(price, num, type)
		// recharge(price, num, type)
	})
	$("#sellSubmit").on('click', function() { // 卖出
		var type = 2 //type订单类型 1买单 2卖单 
		var price = $('#Sellprice').val();
		var num = $('#Sellnum').val();
		if (price.length <= 0) {
			$.toast('请输入卖出价格');
			return false;
		}
		if (num.length <= 0) {
			$.toast('请输入卖出数量');
			return false;
		}
		if (Number(num) < 10) {
			$.toast('卖出数量不少于10');
			return false;
		}
		if (Number(price) > max_price) {
			$.toast('卖出价格不能大于' + max_price);
			return false;
		}
		if (Number(price) < min_price) {
			$.toast('卖出价格不能小于' + min_price);
			return false;
		}
		submit(price, num, type)
	})


	GetOrderList()


	var getMarketData = function() {
		$.apiGet('goods/getMarket', function(res) {
			var datalist = res.data.data.ticker
			getLine(datalist)
			// var list = []
			// for (var i = 0; i < datalist.length; ++i) {
			//     if (datalist[i].symbol == 'btcusdt' || datalist[i].symbol == 'ethusdt' || datalist[i].symbol == 'srpusdt' || datalist[i].symbol == 'diausdt') {

			//         datalist[i].symbol = (datalist[i].symbol.replace("usdt", "")).toUpperCase();
			//         datalist[i].status = datalist[i].rose>0
			//         console.log(' datalist[i].status', datalist[i].status)
			//         list.push(datalist[i])
			//     }
			// }

			// dataRender(list)
		});

		// setTimeout(function(){
		//     getMarketData()
		// },60000);

	}

	// getMarketData()

	var getMarketLine = function() {
		// fetch('https://data.gateio.la/api2/1/pairs',{
		//     method: 'GET',
		//     mode: 'no-cors',

		// })//get请求参数是连接在url上
		//   .then( res => {
		//     console.log('res',res)
		//   })
		//   .catch( err => console.log( err ))
		$.ajax({
			url: 'https://gatecn.hpsop.com/api2/1/pairs',
			type: "get",
			header: 'Access-Control-Allow-Origin:*',
			dataType: 'json',
			success: function(res) {
				console.log('res', res)
			},
			error: function(err) {
				console.log('err', err)
			},
			complete: function(complete) {
				console.log('complete', complete)
			}
		})


	}


	// getMarketLine()

	var getLine = function(List) {
		var productChart3 = echarts.init(document.getElementById('main'));
		var upColor = '#ec0000';
		// var upBorderColor = '#8A0000'; 
		var upBorderColor = 'red';
		var downColor = '#00da3c';
		var downBorderColor = '#008F28';


		// 数据意义：开盘(open)，收盘(close)，最低(lowest)，最高(highest)
		var data0 = splitData(List);



		function splitData(rawData) {
			var categoryData = [];
			var values = []
			for (var i = 0; i < rawData.length; i++) {
				categoryData.push(rawData[i].splice(0, 1)[0]);
				values.push(rawData[i])
			}
			return {
				categoryData: categoryData,
				values: values
			};
		}

		function calculateMA(dayCount) {
			var result = [];
			for (var i = 0, len = data0.values.length; i < len; i++) {
				if (i < dayCount) {
					result.push('-');
					continue;
				}
				var sum = 0;
				for (var j = 0; j < dayCount; j++) {
					sum += data0.values[i - j][1];
				}
				result.push(sum / dayCount);
			}
			return result;
		}



		option = {
			title: {
				// text: '上证指数',
				left: 0
			},
			tooltip: {
				trigger: 'axis',
				axisPointer: {
					type: 'cross'
				}
			},
			legend: {
				data: ['行情']
			},
			grid: {
				left: '12%',
				right: '10%',
				bottom: '15%'
			},
			xAxis: {
				type: 'category',
				data: data0.categoryData,
				scale: true,
				boundaryGap: false,
				axisLine: {
					onZero: false
				},
				splitLine: {
					show: false
				},
				splitNumber: 20,
				min: 'dataMin',
				max: 'dataMax'
			},
			yAxis: {
				scale: true,
				splitArea: {
					show: true
				},
				min: function(value) {
					return value.min - 0.5;
				},
				max: function(value) {
					return value.max + 1;
				}
			},
			dataZoom: [{
					type: 'inside',
					start: 50,
					end: 100
				},
				{
					show: true,
					type: 'slider',
					top: '90%',
					start: 50,
					end: 100
				}
			],
			series: [{
					// name: '行情',
					type: 'candlestick',
					data: data0.values,
					itemStyle: {
						color: upColor,
						color0: downColor,
						borderColor: upBorderColor,
						borderColor0: downBorderColor
					},
					markPoint: {
						label: {
							normal: {
								formatter: function(param) {
									return param != null ? Math.round(param.value) : '';
								}
							}
						},
						data: [
							// {
							//     name: 'XX标点',
							//     coord: ['2013/5/31', -0.2],
							//     value: -0.2,
							//     itemStyle: {
							//         color: 'rgb(41,60,85)'
							//     }
							// },
							// {
							//     name: 'highest value',
							//     type: 'max',
							//     valueDim: 'highest'
							// },
							// {
							//     name: 'lowest value',
							//     type: 'min',
							//     valueDim: 'lowest'
							// },
							// {
							//     name: 'average value on close',
							//     type: 'average',
							//     valueDim: 'close'
							// }
						],
						tooltip: {
							formatter: function(param) {
								return param.name + '<br>' + (param.data.coord || '');
							}
						}
					},
					// markLine: {
					//     symbol: ['none', 'none'],
					//     data: [
					//         [
					//             {
					//                 name: 'from lowest to highest',
					//                 type: 'min',
					//                 valueDim: 'lowest',
					//                 symbol: 'circle',
					//                 symbolSize: 10,
					//                 label: {
					//                     show: false
					//                 },
					//                 emphasis: {
					//                     label: {
					//                         show: false
					//                     }
					//                 }
					//             },
					//             {
					//                 type: 'max',
					//                 valueDim: 'highest',
					//                 symbol: 'circle',
					//                 symbolSize: 10,
					//                 label: {
					//                     show: false
					//                 },
					//                 emphasis: {
					//                     label: {
					//                         show: false
					//                     }
					//                 }
					//             }
					//         ],
					//         {
					//             name: 'min line on close',
					//             type: 'min',
					//             valueDim: 'close'
					//         },
					//         {
					//             name: 'max line on close',
					//             type: 'max',
					//             valueDim: 'close'
					//         }
					//     ]
					// }
				},



			]
		};


		productChart3.setOption(option);

	}

	var getKilne = function(type) {
		//Deal/getKilne
		// $.apiRequest('Deal/getKilne', {
		//     type:type
		// },function (res) {
		//     var List = []
		//     var item = []
		//     // ['2013/1/24', 2320.26,2320.26,2287.3,2362.94],
		//     //开盘(open)，收盘(close)，最低(lowest)，最高(highest)
		//     for(var i = 0 ; i<res.data.length ; i++){
		//         item = []
		//         item.push(res.data[i].create_time)
		//         item.push(Number(res.data[i].open_price).toFixed(2))
		//         item.push(Number(res.data[i].col_price).toFixed(2))
		//         item.push(Number(res.data[i].low_price).toFixed(2))
		//         // if((res.data[i].hig_pairce) == '2300.00000000'|| res.data[i].col_price == '2015.00000000'){
		//         //     continue
		//         // }
		//         item.push(Number(res.data[i].hig_pairce).toFixed(2))
		//         // item.push(Number(res.data[i].wave).toFixed(2))
		//         // item.push(Number(res.data[i].vol).toFixed(2))
		//         List.push(item)
		//     }


		//     getLine(List)
		// })
	}


	$(".time-sel").on('click', 'li', function() {
		var type = $(this).data('type')
		$(".time-sel li").removeClass('on');
		$(this).addClass('on')
		getKilne(type)
	})

});
