// import Event from './event.js'
var pako = window.pako
var data = [{
		"id":1618883260,
		"open":55478.6,
		"close":55530.1,
		"low ":55478.5,
		"high ":55646.7,
		"amount ":588.74,
		"vol ":588740,
		"trade_turnover ":32716158.8796,
		"count":3717,
	},
	{
		"id":1618883320,
		"open":55530,
		"close":55350.6,
		"low":55280.1,
		"high":55580.9,
		"amount":1000.2,
		"vol":1000200,
		"trade_turnover":55414035.0376,
		"count":3438,
	},
	{
		"id":1618883380,
		"open":55350.6,
		"close":55454.7,
		"low":55217.7,
		"high":55454.8,
		"amount":899.33,
		"vol":899330,
		"trade_turnover":49743574.2228,
		"count":3316,
	},
	{
		"id":1618883440,
		"open":55454.7,
		"close":55449.2,
		"low":55333.5,
		"high":55454.7, 
		"amount": 383.91, 
		"vol": 383910, 
		"trade_turnover": 21262076.8532, 
		"count": 2421,
	}, 
	{
		"id": 1618883500,
		"open": 55449.2,
		"close": 55442,
		"low": 55388,
		"high": 55473.1,
		"amount": 652.022,
		"vol": 652022,
		"trade_turnover": 36142429.1204,
		"count": 3477
	}, {
		"id": 1618883560,
		"open": 55442.1,
		"close": 55163.6,
		"low": 55163.5,
		"high": 55445,
		"amount": 370.092,
		"vol": 370092,
		"trade_turnover": 20466511.746,
		"count": 1676
	}, {
		"id": 1618883620,
		"open": 55163.5,
		"close": 55141.7,
		"low": 55125,
		"high": 55249,
		"amount": 384.48,
		"vol": 384480,
		"trade_turnover": 21214932.2792,
		"count": 2149
	}, {
		"id": 1618883680,
		"open": 55141.7,
		"close": 55299.9,
		"low": 55115,
		"high": 55300,
		"amount": 684.456,
		"vol": 684456,
		"trade_turnover": 37772977.9156,
		"count": 2393
	}, {
		"id": 1618883740,
		"open": 55300,
		"close": 55313.3,
		"low": 55228.6,
		"high": 55342.5,
		"amount": 415.076,
		"vol": 415076,
		"trade_turnover": 22952637.9766,
		"count": 1921
	}, {
		"id": 1618883800,
		"open": 55313.5,
		"close": 55415.3,
		"low": 55313.5,
		"high": 55415.3,
		"amount": 416.368,
		"vol": 416368,
		"trade_turnover": 23054633.2262,
		"count": 1211
	}]
var socket = {
	socket: null, // socket name
	realTimeData: null, // 请求实时数据的参数
	intervalObj: null, // 定时器的名字
	lastRealTimeData: null, // 上一次请求的产品
	sendData(historyData, realTimeDatas, history) {
		// 储存历史数据
		this.historyData = historyData
		this.realTimeData = realTimeDatas
		// 如果上一次订阅过产品
		if (this.lastRealTimeData) {
			// 如果不是订阅历史产品 那么肯定就是切换周期咯 或者 切换产品
			// 那么就取消订阅上一次的产品实时数据
			if (!history) {
				console.log('取消订阅'+ this.lastRealTimeData)
				this.sendWsRequest({
				  "unsub": this.lastRealTimeData,
				  "id": "id1"
				})
			}
		
			// 请求这一次的历史
			this.sendWsRequest(this.historyData)
			
			// 如果不是订阅历史产品 那么肯定就是切换周期咯 或者切换产品咯 
			// 那么就订阅一下 这次产品的或者周期的 实时数据
			if (!history) {
				console.log('订阅新的'+ realTimeDatas)
				this.sendWsRequest({
					"sub": realTimeDatas,
					"id": "id1"
				})
			}
		} else {
			// 如果是第一次订阅，就是说刚进入交易所，
			// 先存起来这一次请求的产品 作为历史产品
			this.lastRealTimeData = this.realTimeData
			// 然后 初始化一下websocket
			this.initWs()
		}
	},
	initWs() {
		this.socket = new WebSocket('ws://api.huobiasia.vip/ws')
		this.socket.onopen = () => {
			this.sendWsRequest(this.historyData)
			this.sendWsRequest({
				"sub": this.historyData.req,
				"id": "id1"
			})
		}
		this.socket.onmessage = resp => {
			this.message(resp)
		}
		this.socket.onclose = () => {
			this.close()
		}
		this.socket.onerror = err => {
			this.error(err)
		}
	},
	error(err) {
		console.log(err, 'depth-socket::error')
	},
	close() {
		// 如果websocket关闭的话，就从新打开一下。
		this.initWs()
		console.log('depth-socket::close')
	},
	message(resp) {
		let this_ = this
		let reader = new FileReader()
		reader.onload = function(e) {
			// 对数据进行解压
			let msg = JSON.parse(pako.ungzip(reader.result, {
				to: 'string'
			}))
			
			// console.log(msg)
		
			// 如果是实时数据触发Event('realTime') 喂数据
			if (msg.tick) {
				// for(let i = 0;i<data.length;i++){
					// Event.emit('realTime', data[i])
				// }
				// console.log('msg1',msg.tick)
				
				Event.emit('realTime', msg.tick)
			}
		
			//响应服务器，避免断开连接
			if (msg.ping) {
				this_.socket.send(JSON.stringify({
					pong: msg.ping
				}));
				this_.hasCheck = true
			}
			
			this_.lastRealTimeData = this_.realTimeData
			// 如果是历史数据触发Event('data') 绘制数据
			if (msg.data && Array.isArray(msg.data)) {
				Event.emit('data', msg.data)
			}
		}
		// //将返回的数据解析为字符串格式
		reader.readAsArrayBuffer(resp.data);
	},
	checkSendMessage(options) {
		// 这里处理websocket 连接不上的问题
		var checkTimes = 10
		var i = 0
		this.intervalObj = setInterval(() => {
			i += 1
			if (this.socket.readyState === 1) {
				// ...
				this.socket.send(options)
				clearInterval(this.intervalObj)
				return
			}
			if (i >= checkTimes) {
				clearInterval(this.intervalObj)
				console.log('send post_data_str timeout.')
			}
		}, 500)
	},
	sendWsRequest(options) {
		switch (this.socket.readyState) {
			case 0:
				this.checkSendMessage(JSON.stringify(options))
				break
			case 1:
				this.socket.send(JSON.stringify(options))
				break
			case 2:
				console.log('ws关闭状态')
				break
			case 3:
				this.initWs()
				break
			default:
				console.log('ws未知错误')
		}
	}
}

// export default socket
